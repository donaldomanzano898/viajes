<?php require  'head.php'; ?>
 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active" style="background-image: url('../img/1.jpg')">
        <div class="carousel-caption d-none d-md-block">
             
          <div class="ok"><h1 style="color: #f8b600">Visión</h1></div>
          <div class="bar">
          <p class="lead">Ser una agencia de viajes de turismo reconocida a nivel nacional e internacional, por medio de un servicio de óptima calidad brindando una buena satisfacción de nuestros clientes, generando experiencias, conocimientos y aprendizajes viajando.</p>
          </div>
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('../img/2.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <div class="ok"><h1 style="color: #f8b600">Misión</h1></div>
          <div class="bar"> 
          <p class="lead">Brindar un servicio de calidad y confiabilidad, por medio de los paquetes de viaje que el cliente elije, para superar sus expectativas a través de viajes únicos ofreciendo una buena atención por parte nuestra organización, con precios accesibles</p>
          </div>
        </div>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('../img/3.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h2 class="display-4">Viaja</h2>
          <p class="lead"></p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  </div>
 <section class="price-area section-gap">
        <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-70 col-lg-8">
                        <div class="title text-center">
                            <h1 class="mb-10">"Todo soñador sabe que es perfectamente posible sentir nostalgia por un lugar en el que nunca se ha estado, quizás más nostalgia que por algo conocido"</h1>
                            <p>- Judith Thurman</p>
                        </div>
                    </div>
                </div>            
          
            <div class="">
              <div class="single-price">
                <h4>Valores</h4>
                <ul class="price-list" style="text-align: center;">
                  <li >
                    <span class="price-btn">Pasión por conocer nuevos lugares</span>
                   
                  </li>
                  <li c>
                     <span class="price-btn">Amabilidad con los clientes en todo momento</span>
                  </li>
                  <li >
                    <span class="price-btn">Satisfacción al turista en su viaje</span>
                  </li>
                  <li >
                     <span class="price-btn">Calidad en el servicio de viajes</span>
                  </li>
                  <li >
                    <span class="price-btn">Trabajo en equipo para brindar un servicio completo</span>
                  </li> 
                  <li >
                     <span class="price-btn">Innovación para que el turista genere nuevas experiencias</span>
                  </li>   
                   <li >
                     <span class="price-btn">Compromiso de la organización a través del servicio que brindara</span>
                  </li>   
                   <li >
                     <span class="price-btn">Amor por el trabajo que se hace</span>
                  </li>  
                    <li >
                     <span class="price-btn">Honestidad con nuestros clientes</span>
                  </li>                           
                </ul>
              </div>
           
           
                                 
          </div>
        </div>  
      </section>
   <?php require  'footer.php'; ?>
  <style type="">
    .bar{
 
  height:auto;
  padding-top: 2em;
  opacity: 0.7;
  background: black;
  border-top: 3px solid #ccc;
  border-bottom: 3px solid #ccc;
  margin-top: 5.0em;


}
  </style>

