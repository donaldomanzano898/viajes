
<body> 
   <section class="banner-area relative">
        <div class="overlay overlay-bg"></div>        
        <div class="container">
          <div class="row fullscreen align-items-center justify-content-between">
            <div class="col-lg-6 col-md-6 banner-left">
              <h2 class="text-white">El Viaje de Mil Millas</h2>
              <h1 class="text-white">Comienza en un solo PASO</h1>
              <h3 class="text-white">-Lao Tse</h3>
              <p class="text-white">
                Bintuor es un ajente de viajes donde  podras conocer lo bellos pueblos de México, la belleza de la naturaleza a un costo accesible, paquetes desde $500.00
              </p>
              <a href="#" class="primary-btn btn-danger">Ver Paquetes..</a>
            </div>
            
          </div>
        </div>          
      </section>
      
   


      <!-- Start popular-destination Area -->
      <section class="popular-destination-area section-gap">
        <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-70 col-lg-8">
                        <div class="title text-center">
                            <h1 class="mb-10">Destinos pupulares</h1>
                            <p>Viaja y conoce todos los destinos Turisticos, Extremos, Educativos y mas...</p>
                        </div>
                    </div>
                </div> 

    
          <div class="row">
<?php 
  require_once("controladores/conexion/MySQL.php");
$server=new MySQL();
$datos=$server->EjecutarSQL("Select * from paquetes");

 while($rf = mysqli_fetch_assoc($datos)){
  $idpa=$rf['id_paquete'];
  $destino=$rf['destino'];
  $info=$rf['informacion'];
   $costo=$rf['costo'];
   $imagen=$rf['imagen'];
   $dispo=$rf['disponibles'];
       ?>   
            <div class="col-lg-6">
              <div class="single-destination relative">
                <div class="thumb relative">
                  <div class="overlay overlay-bg"></div>
                  <img class="img-fluid" src="controladores/archivos/<?php echo $imagen;  ?>" alt="">
                </div>
                <div class="desc">  
                  <a href="detallepaquete.php?paquete=<?php echo $idpa ?>" class="price-btn">$ <?php echo $costo ?> Mx</a>      
                  <h4>Boletos disponibles: <?php echo $dispo ?></h4>
                  <p><?php echo $destino ?></p>     
                </div>
              </div>
            </div>
              <?php }?>  
  

          </div>
       
        </div>  
      </section>

       <!-- Start popular-destination Area -->
      <section class="popular-destination-area section-gap">
        <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-70 col-lg-8">
                        <div class="title text-center">
                            <h1 class="mb-10">Destinos a Playas</h1>
                            <p>Llega el varano y con èl las ganas de pisar la arena y sentir las olas del mar mecièndonos</p>
                        </div>
                    </div>
                </div> 

    
          <div class="row">
<?php 


$datosplaya=$server->EjecutarSQL("Select * from paquetes WHERE tipo_paquete='Playa'");

 while($rf = mysqli_fetch_assoc($datosplaya)){
  $destino=$rf['destino'];
  $info=$rf['informacion'];
   $costo=$rf['costo'];
   $imagen=$rf['imagen'];
   $dispo=$rf['disponibles'];
       ?>   
            <div class="col-lg-6">
              <div class="single-destination relative">
                <div class="thumb relative">
                  <div class="overlay overlay-bg"></div>
                  <img class="img-fluid" src="controladores/archivos/<?php echo $imagen;  ?>" alt="">
                </div>
                <div class="desc">  
                  <a href="#" class="price-btn">$ <?php echo $costo ?> Mx</a>      
                  <h4><?php echo $info ?></h4>
                  <p><?php echo $destino ?></p>   
                      <h4>Boletos disponibles: <?php echo $dispo ?></h4>  
                </div>
              </div>
            </div>
              <?php }?>  
  

          </div>
       
        </div>  
      </section>

       <!-- Start popular-destination Area -->
      <section class="popular-destination-area section-gap">
        <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-70 col-lg-8">
                        <div class="title text-center">
                            <h1 class="mb-10">Destinos Pueblos Magicos</h1>
                            <p>Vijar es descubrir eselugar al cual perteneces.</p>
                        </div>
                    </div>
                </div> 

    
          <div class="row">
<?php 


$datospueblo=$server->EjecutarSQL("Select * from paquetes WHERE tipo_paquete='pueblo_magico'");

 while($rf = mysqli_fetch_assoc($datospueblo)){
  $destino=$rf['destino'];
  $info=$rf['informacion'];
   $costo=$rf['costo'];
   $imagen=$rf['imagen'];
       ?>   
            <div class="col-lg-6">
              <div class="single-destination relative">
                <div class="thumb relative">
                  <div class="overlay overlay-bg"></div>
                  <img class="img-fluid" src="controladores/archivos/<?php echo $imagen;  ?>" alt="">
                </div>
                <div class="desc">  
                  <a href="#" class="price-btn">$ <?php echo $costo ?> Mx</a>      
                  <h4><?php echo $info ?></h4>
                  <p><?php echo $destino ?></p>     
                </div>
              </div>
            </div>
              <?php }?>  
  

          </div>
       
        </div>  
      </section>
        <div class="row">
        <div class="container"> 
       
          <div class="col-md-12" align="center"> 
            <iframe width="560" height="315" src="https://www.youtube.com/embed/wwIEvRPTvM4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
          </div>
        </div>
         </div>

</body>
