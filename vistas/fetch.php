<?php
include('connect.php');
if (isset($_POST['view'])) {
    
    if ($_POST["view"] != '') {
        $update_query = "UPDATE comments SET comment_status = 1 WHERE comment_status=0";
        mysqli_query($con, $update_query);
    }
    $paque=$_POST['paque'];
     $user=$_POST['user'];
    
    $query  = "SELECT comments.*,solicitud.* FROM comments, solicitud WHERE  solicitud.status='aceptado'and solicitud.id_usuarios=$user and solicitud.id_paquetes=$paque ORDER BY comment_id DESC LIMIT 5";
    $result = mysqli_query($con, $query);
    $output = '';
    
    if (mysqli_num_rows($result) > 0) {
        
        while ($row = mysqli_fetch_array($result)) {
            
            $output .= '
  <li>
  <a href="view.php?id=' . $row["comment_id"] . '">
  <strong>' . $row["comment_subject"] . '</strong><br />
  <small><em>' . $row["comment_text"] . '</em></small>
  </a>
  </li>

  ';
        }
    }
    
    else {
        $output .= '<li><a href="#" class="text-bold text-italic">No hay notificaciones</a></li>';
    }
    
    $status_query = "SELECT comments.*,solicitud.* FROM comments, solicitud WHERE  solicitud.status='aceptado'and solicitud.id_usuarios=$user and solicitud.id_paquetes=$paque AND comment_status=0";
    $result_query = mysqli_query($con, $status_query);
    $count        = mysqli_num_rows($result_query);
    
    $data = array(
        'notification' => $output,
        'unseen_notification' => $count
    );
    
    echo json_encode($data);
}
?>