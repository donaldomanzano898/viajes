<?php 
ob_start();
include 'head.php';
require_once("controladores/conexion/MySQL.php");
if (isset($_SESSION["id"])){
        $iduser= $_SESSION["id"];
        
    }else{
 header('Location: login.php');//Aqui lo redireccionas al lugar que quieras.
     die() ;

    }
 ?>
 <header class="bg-dark text-center py-5 mb-4" style="height: 200px">
  <div class="container ">
    <br>  <br>  <br>  
    <h1 class="font-weight-light text-white">Conocenos</h1>
  </div>
</header>
<div class="container">	

	<div class="jumbotron">

  <h1 class="display-4">Este es un apartado de BinTour donde tú como usuario puedes proponer viajes a diferentes detinos.</h1>
   <?php  if (isset($_GET['estado'])) {
    if ($_GET['estado']=='correcto') {
      echo '<div class="alert alert-success" role="alert">Gracias por contestar, tomaremos en cuenta tu respuesta</div>';
      # code...
    }else if($_GET['estado']=='repetido'){
        echo '<div class="alert alert-warning" role="alert">Ya contestaste esta propuesta!</div>';
     # code...
    }
   } ?>

  

  <?php   
$server=new MySQL();
$datos=$server->EjecutarSQL("Select * from propuestas where fecha_limite>=now()");
  while($rf = mysqli_fetch_assoc($datos)){
  $id_propuesta=$rf['id_propuesta'];
  $propuesta=$rf['propuesta_destino'];
  $opcion1=$rf['opcion1'];
  $opcion2=$rf['opcion2'];
  $opcion3=$rf['opcion3'];
  $fecha_limite=$rf['fecha_limite'];

                                                   
   ?> 
   <hr class="my-4"> 
  <p class="lead"><?php   echo $propuesta;  ?></p>
  <p>Esta propuesta finaliza el <?php   echo $fecha_limite; ?></p>
  <hr class="my-4">
  <form method="POST" action="./controladores/agregarpropuestauser.php">  
    <input type="text" value="<?php echo $id_propuesta; ?>" name="id_propuesta" hidden/>
    <input type="text" value="<?php echo $_SESSION["id"];; ?>" name="user" hidden/>
  <div class="custom-control custom-radio">
  <input type="radio" id="customRadio1<?php echo $id_propuesta; ?>" name="pro<?php echo $id_propuesta; ?>" class="custom-control-input ggx" value="1" required>
  <label class="custom-control-label" for="customRadio1<?php echo $id_propuesta; ?>"><?php  echo $opcion1; ?></label>
</div>
<div class="custom-control custom-radio">
  <input type="radio" id="customRadio2<?php echo $id_propuesta; ?>" name="pro<?php echo $id_propuesta; ?>" class="custom-control-input ggx" value="2">
  <label class="custom-control-label" for="customRadio2<?php echo $id_propuesta; ?>"><?php  echo $opcion2; ?></label>
</div>
<div class="custom-control custom-radio">
  <input type="radio" id="customRadio3<?php echo $id_propuesta; ?>" name="pro<?php echo $id_propuesta; ?>" class="custom-control-input ggx" value="3">
  <label class="custom-control-label" for="customRadio3<?php echo $id_propuesta; ?>"><?php  echo $opcion3; ?></label>
</div>
<div class="custom-control custom-radio">
  <input type="radio" id="customRadio4<?php echo $id_propuesta; ?>" name="pro<?php echo $id_propuesta; ?>" class="custom-control-input ggx" value="4">
  <label class="custom-control-label" for="customRadio4<?php echo $id_propuesta; ?>">Otro</label>
</div>
<div class="custom-control ">
  <label class="control-form" for="otrop">Especifique destino: </label>
  <input type="text" name="otro"  id="hhx" class="control-form " readonly="true" />
  
</div>
  
  <p class="lead">
    <input class="btn btn-primary btn-lg" type="submit"  value="Enviar" />
    </form>
  </p>
<?php   } ?>
</div>
</div>

 <?php 	
include  'footer.php';
ob_end_flush(); 
 ?>

 <script type="text/javascript">
   $(".ggx").change(function() {
    var valor = $(this).val();
 
 
    if (valor == '4') {
        $("#hhx").attr("readonly", false);
        $("#hhx").attr("required", true);
    } else {
        $("#hhx").attr("readonly", true);
        $("#hhx").attr("required", false);
        

    }
});
 </script>