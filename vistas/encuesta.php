
  
  <h2>Selecciona la opcion que creas corresponde a la valoracion del servicio durante el viaje</h2>
  <table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col"></th>
        <th scope="col">Pesimo</th>
        <th scope="col">Malo</th>
        <th scope="col">Regular</th>
        <th scope="col">Bueno</th>
        <th scope="col">Excelente</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">¿Que te pareció el paquete ofrecido?</th>
          <td>
          <div class="funkyradio">
            <div class="funkyradio-danger">
                <input type="radio" name="radpreg1" id="radio1" required />
                <label for="radio1">Pesimo</label>
              </div>
            </div>
          </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-warning">
                <input type="radio" name="radpreg1" id="radio2" />
                <label for="radio2">Malo</label>
              </div>
            </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-default">
                <input type="radio" name="radpreg1" id="radio3" />
                <label for="radio3">Regular</label>
              </div>
            </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-info">
                <input type="radio" name="radpreg1" id="radio4" />
                <label for="radio4">Bueno</label>
              </div>
            </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-success">
                <input type="radio" name="radpreg1" id="radio5" />
                <label for="radio5">Excelente</label>
              </div>
            </div>
          </td>
      </tr>
      <tr>
        <th scope="row">¿Que te pareció el transporte?</th>
          <td>
          <div class="funkyradio">
            <div class="funkyradio-danger">
                <input type="radio" name="radpreg2" id="radio6"  required />
                <label for="radio6">Pesimo</label>
              </div>
            </div>
          </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-warning">
                <input type="radio" name="radpreg2" id="radio7" />
                <label for="radio7">Malo</label>
              </div>
            </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-default">
                <input type="radio" name="radpreg2" id="radio8" />
                <label for="radio8">Regular</label>
              </div>
            </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-info">
                <input type="radio" name="radpreg2" id="radio9" />
                <label for="radio9">Bueno</label>
              </div>
            </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-success">
                <input type="radio" name="radpreg2" id="radio10" />
                <label for="radio10">Excelente</label>
              </div>
            </div>
          </td>
      </tr>
      <tr>
        <th scope="row">¿Cómo valorarias el hospedaje?</th>
          <td>
          <div class="funkyradio">
            <div class="funkyradio-danger">
                <input type="radio" name="radpreg3" id="radio11" required />
                <label for="radio11">Pesimo</label>
              </div>
            </div>
          </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-warning">
                <input type="radio" name="radpreg3" id="radio12" />
                <label for="radio12">Malo</label>
              </div>
            </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-default">
                <input type="radio" name="radpreg3" id="radio13" />
                <label for="radio13">Regular</label>
              </div>
            </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-info">
                <input type="radio" name="radpreg3" id="radio14" />
                <label for="radio14">Bueno</label>
              </div>
            </div>
          </td>
          <td>
            <div class="funkyradio">
              <div class="funkyradio-success">
                <input type="radio" name="radpreg3" id="radio15" />
                <label for="radio15">Excelente</label>
              </div>
            </div>
          </td>
      </tr>
      <tr>
        <th scope="row">¿Que te parecieron los recorridos?</th>
        <td>
          <div class="funkyradio">
            <div class="funkyradio-danger">
              <input type="radio" name="radpreg4" id="radio16" required />
              <label for="radio16">Pesimo</label>
            </div>
          </div>
        </div>
      </td>
      <td>
        <div class="funkyradio">
          <div class="funkyradio-warning">
            <input type="radio" name="radpreg4" id="radio17" />
            <label for="radio17">Malo</label>
          </div>
        </div>
      </td>
      <td>
        <div class="funkyradio">
          <div class="funkyradio-default">
            <input type="radio" name="radpreg4" id="radio18" />
            <label for="radio18">Regular</label>
          </div>
        </div>
      </td>
      <td>
        <div class="funkyradio">
          <div class="funkyradio-info">
            <input type="radio" name="radpreg4" id="radio19" />
            <label for="radio19">Bueno</label>
          </div>
        </div>
      </td>
      <td>
        <div class="funkyradio">
          <div class="funkyradio-success">
            <input type="radio" name="radpreg4" id="radio20" />
            <label for="radio20">Excelente</label>
          </div>
        </div>
      </td>
      </tr>

    </tbody>


  </table>
<h3>En general, ¿Cómo calificarias el viaje?</h3>

  <p class="clasificacion">
    <input id="cal1" type="radio" name="estrellas" value="1" required><!--
    --><label for="cal1">★</label><!--
    --><input id="cal2" type="radio" name="estrellas" value="2"><!--
    --><label for="cal2">★</label><!--
    --><input id="cal3" type="radio" name="estrellas" value="3"><!--
    --><label for="cal3">★</label><!--
    --><input id="cal4" type="radio" name="estrellas" value="4"><!--
    --><label for="cal4">★</label><!--
    --><input id="cal5" type="radio" name="estrellas" value="5"><!--
    --><label for="cal5">★</label>
  </p>

  <div class="custom-control custom-radio">
  <input type="checkbox" id="custo" name="pro" class="custom-control-input ggx" value="4">
  <label class="custom-control-label" for="custo">Dar una opinion y subir una foto</label>
</div>
<div class="custom-control ">
  <label class="control-form" for="otrop">Opinion: </label>
  <textarea type="text" name="opinion"  id="hhx" class="control-form " maxlength="200" readonly="true" ></textarea>
  <input type="file" name="imagen" class="form-control " id="file-upload" accept="image/*" required/>
  
</div>

<script type="">
    $(".ggx").change(function() {
    var valor = $(this).val();
 
 
    if (valor == '4') {
        $("#hhx").attr("readonly", false);
        $("#hhx").attr("required", true);
         $("#hhxx").attr("readonly", false);
        $("#hhxx").attr("required", true);
    } else {
        $("#hhx").attr("readonly", true);
        $("#hhx").attr("required", false);
         $("#hhxx").attr("readonly", true);
        $("#hhxx").attr("required", false);
        

    }
});
</script>