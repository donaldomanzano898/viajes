<?php 
ob_start();
include_once 'head.php';
require_once("controladores/conexion/MySQL.php");
if (isset($_SESSION["id"])){
        $iduser= $_SESSION["id"];
    }else{
 header('Location: login.php');//Aqui lo redireccionas al lugar que quieras.
     die() ;

    }
 ?>
 <?php if (isset($_GET['paquete'])) {
 $id_paquete=$_GET['paquete'];
 $server=new MySQL();
 $solsql="SELECT * FRom paquetes WHERE id_paquete='$id_paquete'";
 $so=$server->EjecutarSQL($solsql);
	 while($rsoli = mysqli_fetch_assoc($so)){			      			
	   $destino=$rsoli['destino'];
								
								}
 }else{
 	echo '<div class="alert alert-danger" role="alert">No Se encontro el paquete</div>';
 	$destino='Paquete 1';
 } ?>

<header class="bg-dark text-center py-5 mb-4" style="height: 200px">
  <div class="container ">
    <br>  <br>  <br>  
    <h1 class="font-weight-light text-white">Detalle del paquete <?php echo $destino; ?> </h1>
  </div>
</header>
<div class="container">

          <div class="row">
<?php 


$datos=$server->EjecutarSQL("Select * from paquetes WHERE id_paquete='$id_paquete'");

 while($rf = mysqli_fetch_assoc($datos)){
  $idpa=$rf['id_paquete'];
  $destino=$rf['destino'];
  $info=$rf['informacion'];
   $costo=$rf['costo'];
   $imagen=$rf['imagen'];
   $dispo=$rf['disponibles'];
   $fecha_salida=$rf['fecha_salida'];
   $fecha_regreso=$rf['fecha_regreso'];
       ?>   
            <div class="col-lg-6">
              <div class="single-destination relative">
                <div class="thumb relative">
                  <div class="overlay overlay-bg"></div>
                  <img class="img-fluid" src="controladores/archivos/<?php echo $imagen;  ?>" alt="">
                </div>
                <div class="desc">  
                  <a href="detallepaquete.php?paquete=<?php echo $idpa ?>" class="price-btn">$ <?php echo $costo ?> Mx</a>      
                  <h4><?php echo $dispo ?></h4>
                  <p><?php echo $destino ?></p>     
                </div>
              </div>
            </div>
            <div class=" col-lg-6">
            	<div class="alert alert-info"> Detalles del paquete <b><?php echo $destino; ?></b></div>
            	<hr>
            	<div class="container">
            		<p>
            		Fecha de salida: <?php echo $fecha_salida; ?>
            		<br>
            		Fecha de llegada: <?php echo $fecha_regreso; ?>
            		<h2></h2>
            		<br>
            		<?php echo $info; ?>
            	</p>
            	<p>
            		Pago en <b>BBVA Bancomer</b>:
4152-3134-2067-7200

            	</p>
            	<hr>
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus-square"></i>
 			Subir Referencia de pago</button>
            	</div>
            	
            </div>
            <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="./controladores/pago.php" method="POST" enctype="multipart/form-data">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Subir Referencia de Pago</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

	<div>
		
		<input type="text" value="<?php echo $idpa; ?>"  hidden name="id_paquete"/>
		<input type="text" value="<?php echo 		$_SESSION["id"]?>"  hidden name="id_usuario"/> 
    <input type=" text" id="scosto" value="<?php  echo $costo; ?> " hidden>
    <label>Cantidad de boletos</label>
    <input type="number" name="cantidad" id="boletoscantidad" class="form-control" min="1" max="<?php  echo $dispo ?>" required />
    <label> Su pago debe de ser de:</label>
    <input type="number" id="resco" class="form-control price-btn" value="" />
		<label>Referencia</label>
		<input type="file" name="pago" class="form-control " id="file-upload"  required/>
		<input type="checkbox" required><a href="#">Acepto Terminos y condiciones</a>

	</div>
	<hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
      </div>
      </form>
    </div>
  </div>
</div>  

              <?php }?>  
  

          </div>
          <script type="text/javascript">
            $(document).ready(function(){
        var consulta;
        //hacemos focus al campo de búsqueda
        $("#boletoscantidad").focus();
                                                                                                     
        //comprobamos si se pulsa una tecla
        $("#boletoscantidad").keyup(function(e){
                                      
              //obtenemos el texto introducido en el campo de búsqueda
              cantidad = $("#boletoscantidad").val();
              scosto = $("#scosto").val();
              res= cantidad*scosto;
                $("#resco").attr("value", res);
          
        });                                                     
}); 
          </script>

</div>

 <?php
include_once 'footer.php';
ob_end_flush();
 ?>

