<div class="container">
    <div class="row">
         <div class="col-md-8">
        <blockquote class="blockquote text-center mb-0">
          <svg class="lnr text-muted quote-icon pull-left">
            <use xlink:href="#lnr-heart">                                       
          </use></svg>
          <p class="mb-0">Nada desarrolla tanto la inteligencia como viajar!</p>
          <footer class="blockquote-footer">Emile Zola
          </footer>
        </blockquote>
        </div>
        <div class="col-md-4">
        <a  class="flot-right btn btn-white border-0 rounded shadow post-pager-link p-0 next ml-4" href="">
               <span class="d-flex h-100">
              <span class="p-3 d-flex flex-column justify-content-center w-100">
               
            <div class="indicator mb-1 text-uppercase text-muted"> <a href="../index.php">Ver todos los paquetes<i class="fa fa-bars ml-2"></i></a></div>
                 
                     </span>
               <span class="bg-primary p-2 d-flex align-items-center text-white">
               <i class="fa fa-arrow-circle-right"></i>
          </span>
         </span>
</a></div>
        </div>
        
      </div>
<!-- Grid row -->
            <div class="row">
            
                <!-- Grid column -->
                <div class="col-md-12">
                    
                    <!--Footer-->
                    <footer class="page-footer negro center-on-small-only pt-0">

                        <!--Footer Links-->
                        <div class="container">

                            <!--Grid row-->
                            <div class="row pt-5 mb-3 text-center d-flex justify-content-center">

                                <!--Grid column-->
                                <div class="col-md-2 mb-3">
                                    <h6 class="title font-bold"><a href="acercade.php">Acerca de...</a></h6>
                                </div>
                                <!--Grid column-->

                                <!--Grid column-->
                                <div class="col-md-2 mb-3">
                                    <h6 class="title font-bold"><a href="index.php">Paquetes</a></h6>
                                </div>
                                <!--Grid column-->

                                <!--Grid column-->
                                <div class="col-md-2 mb-3">
                                    <h6 class="title font-bold"><a href="experiencias.php">Valoración y Experiencias</a></h6>
                                </div>
                                <!--Grid column-->

                                <!--Grid column-->
                               
                                <!--Grid column-->

                                <!--Grid column-->
                                <div class="col-md-2 mb-3">
                                    <h6 class="title font-bold"><a href="contacto.php">Contacto</a></h6>
                                </div>
                                <!--Grid column-->

                            </div>
                            <!--Grid row-->

                            <hr class="rgba-white-light" style="margin: 0 15%;">

                            <!--Grid row-->
                            <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

                                <!--Grid column-->
                                <div class="col-md-8 col-12 mt-5">
                                    <p style="line-height: 1.7rem">Brindar un servicio de calidad y confiabilidad, por medio de los paquetes de viaje que el cliente elije, para superar sus expectativas a través de viajes únicos ofreciendo una buena atención por parte nuestra organización, con precios accesibles.   </p>
                                    

                                </div>
                                <!--Grid column-->

                            </div>
                            <!--Grid row-->

                            <hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">

                            <!--Grid row-->
                            <div class="row pb-3">

                                <!--Grid column-->
                                <div class="col-md-12">

                                    <div class="footer-socials mb-5 flex-center">
                                        <!--Facebook-->
                                        <a href="https://www.facebook.com/Bintour-2309663692425766/" class="icons-sm fb-ic"><i class="fab fa-facebook fa-lg white-text mr-md-4"> </i></a>
                        
                                        <!--Instagram-->
                                        <a class="icons-sm ins-ic"><i class="fab fa-instagram fa-lg white-text mx-md-4"> </i></a>
                                    
                                    </div>
                                </div>
                                <!--Grid column-->
                            </div>
                            <!--Grid row-->

                        </div>
                        <!--/Footer Links-->

                        <!--Copyright-->
                        <div class="footer-copyright">
                            <div class="container-fluid">
                                <?php   echo date("Y"); ?>Copyright: <a href="#">BinTour.com</a>
                            </div>
                        </div>
                        <!--/Copyright-->

                    </footer>
                    <!--/Footer-->
                                    
                </div>
                <!-- Grid column -->
            
            </div>
            <!-- Grid row -->
           </html>