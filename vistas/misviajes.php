<?php session_start(); 
ob_start();?>

<!DOCTYPE html>
<html>
<meta   charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="../OPENCODE_files/main.css">
    
    <link rel="shortcut icon" href="../OPENCODE_files/o.png">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css"> 
<link rel="stylesheet" type="text/css" href="../css/fot.css"> 
<link rel="stylesheet" type="text/css" href="../css/misestilos.css"> 
<link rel="stylesheet" type="text/css" href="../css/mystyle.css">   
<link rel="stylesheet" href="../font/css/all.css"/>
<link rel="shortcut icon"  href="../img/logo1.png">


  <link rel="stylesheet" href="../css/linearicons.css">
      <link rel="stylesheet" href="../css/font-awesome.min.css">
      <link rel="stylesheet" href="../css/magnific-popup.css">
      <link rel="stylesheet" href="../css/jquery-ui.css">        
      <link rel="stylesheet" href="../css/nice-select.css">              
      <link rel="stylesheet" href="../css/animate.min.css">
      <link rel="stylesheet" href="../css/owl.carousel.css">       
      <link rel="stylesheet" href="../css/main.css">
      <link rel="stylesheet" type="text/css" media="screen" href="../css/estilo.css"/>



<script src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/loader.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/popper.min.js"></script>  
<script src="../js/svgembedder.min.js"></script>     
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>   
<script src="../js/jquery-ui.js"></script>         
<script src="../js/easing.min.js"></script>      
<script src="../js/hoverIntent.js"></script>
<script src="../js/superfish.min.js"></script> 
<script src="../js/jquery.ajaxchimp.min.js"></script>
<script src="../js/jquery.magnific-popup.min.js"></script>           
<script src="../js/jquery.nice-select.min.js"></script>          
<script src="../js/owl.carousel.min.js"></script>              
<script src="../js/mail-script.js"></script> 
<script src="../js/main.js"></script>



<head>
  <title>BINTOUR</title>
</head>


  <header id="header">
    
        <div class="header-top">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-6 col-sm-6 col-6 header-top-left">
                <ul>
                  <li><a href="#">Pueblos Magicos</a></li>
                  <li><a href="#">Viajes Redondos</a></li>
                
     
      
   
      <li class="dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <span class="fas fa-pill fa-danger count" style="border-radius:10px;"></span> 
        <span class="fas fa-bell" style="font-size:18px;"></span></a>
       <ul class="dropdown-menu"></ul>
      </li>
                </ul> 

              </div>
              <div class="col-lg-6 col-sm-6 col-6 header-top-right">
              <div class="header-social">
                <a href="https://www.facebook.com/Bintour-2309663692425766/"><i class="fab fa-facebook"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
             
              </div>
              </div>

            </div>                  
          </div>
        </div>
        <div class="container main-menu">
          <div class="row align-items-center justify-content-between d-flex">
              <div id="logo">
                <a href="index.php"><img src="../img/logo.png" alt="" title="" /></a>
              </div>
              <nav id="nav-menu-container">

                <ul class="nav-menu">


                  <li><a href="index.php">Inicio <i class="fas fa-home"></i></a></li>
                  <li><a href="noti.php">Noticias</a><i class="fas fa-newspaper-o"></i></li>
                  <li><a href="acercade.php">Acerca de <i class="fas fa-question-circle">  </i></a></li>
                  <li><a href="misviajes.php">Mis Viajes<i class="fas fa-plane"></i></a></li>
                  <?php if (isset($_SESSION["id"])): ?>
                    <?php    $iduser= $_SESSION["id"];
                     if ($iduser==2): ?>
                      
                  <li><a href="nuevopaquete.php">Paquetes <i class="fas fa-archive"> </i></a></li>
                    <?php endif ?>
      
                    
                  <?php endif ?>
                  <li><a href=""></a></li>
                  <li><a href="contacto.php">Contacto</a></li>
                  <li><a href="propuesta.php">Proponer Viaje</a></li>
    
                    <?php   
       
                      if (isset($_SESSION['usuario'])) {
                      
                 ?> 
                  <li>  <a href=" "><?php echo $_SESSION['usuario']; ?></a></li>
                  <li>  <a href="controladores/cerrar.php">Cerrar<i class=" fas fa-power-off">  </i></a> </li>
                 <?php  

                    } else{?>
                   <li><a href="login.php">Iniciar sesión</a></li>
                   <li><a href="registro.php">Registrar</a></li>
                <?php   } ?>

                </ul>

              </nav><!-- #nav-menu-container -->                      
          </div>
        </div>
      </header><!-- #header -->

       

<script>
$(document).ready(function(){
 
 function load_unseen_notification(view = '')
 {
  var paque=1;
  var user=<?php  if (isset($_SESSION['id'])) {
    echo $_SESSION['id'];
  }else{
  echo "1"; }?>;
  $.ajax({
   url:"fetch.php",
   method:"POST",
   data:{view:view,paque:paque,user:user},
   dataType:"json",
   success:function(data)
   {
    $('.dropdown-menu').html(data.notification);
    if(data.unseen_notification > 0)
    {
     $('.count').html(data.unseen_notification);
    }
   }
  });
 }
 
 load_unseen_notification();
 
 $('#comment_form').on('submit', function(event){
  event.preventDefault();
  if($('#subject').val() != '' && $('#comment').val() != '')
  {
   var form_data = $(this).serialize();
   $.ajax({
    url:"insert.php",
    method:"POST",
    data:form_data,
    success:function(data)
    {
     $('#comment_form')[0].reset();
     load_unseen_notification();
    }
   });
  }
  else
  {
    alert('Campos Obligatorios');
  }
 });
 
 $(document).on('click', '.dropdown-toggle', function(){
  $('.count').html('');
  load_unseen_notification('yes');
 });
 
 setInterval(function(){ 
  load_unseen_notification();; 
 }, 5000);
 
});
</script>
<?php 	
ob_start();

require_once("controladores/conexion/MySQL.php");
if (isset($_SESSION["id"])){
        $iduser= $_SESSION["id"];
    }else{
 header('Location: login.php');//Aqui lo redireccionas al lugar que quieras.
     die() ;

    }

$server=new MySQL();
?>
<header class="bg-dark text-center py-5 mb-4" style="height: 200px">
  <div class="container ">
    <br>  <br>  <br>  
    <h1 class="font-weight-light text-white">Mis Viajes</h1>
  </div>
</header>


<div class="container">	
   <?php  if (isset($_GET['estado'])) {
    if ($_GET['estado']=='correcto') {
      echo '<div class="alert alert-success" role="alert">Gracias por contestar, tomaremos en cuenta tu respuesta</div>';
      # code...
    }else if($_GET['estado']=='repetido'){
        echo '<div class="alert alert-warning" role="alert">Ya contestaste esta Encuesta!</div>';
     # code...
    }
   } ?>
		<table class="table">	
   <thead>	
	  <tr>	
		<th>No.</th>
		<th>Destino</th>
		<th>Fecha Salida</th>
		<th>Informacion</th>
		<th>Estado</th>
		<th>Encuesta</th>
    <th>Descargar Boleto</th>
	  </tr>
	</thead>
<tbody>	
	
	<?php 	
$datos=$server->EjecutarSQL("SELECT DISTINCT solicitud.* , usuarios.*, paquetes.* from paquetes, usuarios,solicitud WHERE solicitud.id_usuarios=usuarios.id and solicitud.id_paquetes=paquetes.id_paquete and usuarios.id=$iduser ");
$i=1;
 while($rf = mysqli_fetch_assoc($datos)){
 	  $usuario=$rf['usuario'];
    $id_usuarios=$rf['id_usuarios'];
$idu=$rf['id'];
      $id_paquete=$rf['id_paquete'];
	  $estado=$rf['estado'];
	  $destino=$rf['destino'];
    $usuario=$rf['usuario'];
	  $info=$rf['informacion'];
	  $fechasalida=$rf['fecha_salida'];	
    $fecha_regreso=$rf['fecha_regreso']; 
       $cantidad_boletos=$rf['cantidad_boletos']; 
	  $estado=$rf['status'];	
 ?>
<tr class="<?php 	if($estado=='espera'){ echo 'btn-warning';}else if($estado=='aceptado'){echo 'btn-success';}else if($estado=='rechazado'){ echo 'btn-danger';} ?>">
		<td>	<?php echo $i; ?></td>
		<td>	<?php 	echo $destino; ?></td>
		<td>	<?php 	echo $fechasalida; ?></td>
		<td>	<?php 	echo $info; ?></td>
		<td>	<?php 	echo $estado; ?></td>
    <td>
		<?php if ($estado=='aceptado'): ?>
       <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ok<?php echo $id_paquete;   ?>">Realizar</button>
<div class="modal fade bd-example-modal-lg" id="ok<?php echo $id_paquete;?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form method="post" action="./controladores/agregarencuesta.php" enctype="multipart/form-data"> 
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Encuesta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <input type="text" value="<?php echo $id_paquete; ?>" name="id_paquete" hidden />
           <input type="text" value="<?php echo $iduser; ?>" name="user" hidden />
<?php include 'encuesta.php'; ?>
        <hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
      </div>
      </form>
      </div>
  </div>
</div> 
    <?php endif ?>
</td>
<td>
  <?php if ($estado=='aceptado'): ?>
   <a href="excel.php?folio=<?php echo $id_paquete."".$idu ?>&nombre=<?php echo $usuario ?>&destino=<?php echo $destino; ?>&fechasalida=<?php echo $fechasalida ?>&personas=<?php echo $cantidad_boletos ?>" class="btn-success btn"><i class="fas fa-file-download"></i></a>
  <?php endif ?>

</td>

</tr>
<?php 	$i++;
} ?>
</tbody>
</table>











</div>

 <?php 	include_once 'footer.php';
 ob_end_flush();  ?>