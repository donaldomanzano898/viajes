<?php
	
	require 'Classes/PHPExcel/IOFactory.php';

	$folio = $_GET['folio'];
	$nombre = $_GET['nombre'];
	$destino = $_GET['destino'];
	$fechasalida = $_GET['fechasalida'];
	$personas = $_GET['personas'];
	


	$objPHPExcel = new PHPExcel();

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel=$objReader->load('plantillaboleto.xlsx');
	$objPHPExcel->setActiveSheetIndex(0);

	$objPHPExcel->getActiveSheet()->SetCellValue('C6', $nombre);
	$objPHPExcel->getActiveSheet()->SetCellValue('C8', $destino);
	$objPHPExcel->getActiveSheet()->SetCellValue('C10', $fechasalida);
	$objPHPExcel->getActiveSheet()->SetCellValue('E10', $personas);
	$objPHPExcel->getActiveSheet()->SetCellValue('E3', $folio);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('boleto.xlsx');

	$basefichero=basename("boleto.xlsx");
	header("Content-Type:application/octet-stream");
	header("Content-Length:".filesize("boleto.xlsx"));
	header("Content-Disposition:attachment; filename=".$basefichero."");
	readfile("boleto.xlsx");



?>