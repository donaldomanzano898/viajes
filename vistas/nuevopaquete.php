
<?php 
ob_start();
require_once("controladores/conexion/MySQL.php");
session_start(); ?>
<!DOCTYPE html>
<html>
<meta   charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="../OPENCODE_files/main.css">
    
    <link rel="shortcut icon" href="../OPENCODE_files/o.png">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css"> 
<link rel="stylesheet" type="text/css" href="../css/fot.css"> 
<link rel="stylesheet" type="text/css" href="../css/misestilos.css"> 
<link rel="stylesheet" type="text/css" href="../css/mystyle.css">   
<link rel="stylesheet" href="../font/css/all.css"/>
<link rel="shortcut icon"  href="../img/logo1.png">


  <link rel="stylesheet" href="../css/linearicons.css">
      <link rel="stylesheet" href="../css/font-awesome.min.css">
      <link rel="stylesheet" href="../css/magnific-popup.css">
      <link rel="stylesheet" href="../css/jquery-ui.css">        
      <link rel="stylesheet" href="../css/nice-select.css">              
      <link rel="stylesheet" href="../css/animate.min.css">
      <link rel="stylesheet" href="../css/owl.carousel.css">       
      <link rel="stylesheet" href="../css/main.css">
      <link rel="stylesheet" type="text/css" media="screen" href="../css/estilo.css"/>


<script src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/loader.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/popper.min.js"></script>  
<script src="../js/svgembedder.min.js"></script>     
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>   
<script src="../js/jquery-ui.js"></script>         
<script src="../js/easing.min.js"></script>      
<script src="../js/hoverIntent.js"></script>
<script src="../js/superfish.min.js"></script> 
<script src="../js/jquery.ajaxchimp.min.js"></script>
<script src="../js/jquery.magnific-popup.min.js"></script>           
<script src="../js/jquery.nice-select.min.js"></script>          
<script src="../js/owl.carousel.min.js"></script>              
<script src="../js/mail-script.js"></script> 
<script src="../js/main.js"></script>



<head>
  <title>BINTOUR</title>
</head>


  <header id="header">
    
        <div class="header-top">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-6 col-sm-6 col-6 header-top-left">
                <ul>
                  <li><a href="#">Pueblos Magicos</a></li>
                  <li><a href="#">Viajes Redondos</a></li>
                
     
      
   
      <li class="dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <span class="fas fa-pill fa-danger count" style="border-radius:10px;"></span> 
        <span class="fas fa-bell" style="font-size:18px;"></span></a>
       <ul class="dropdown-menu"></ul>
      </li>
                </ul> 

              </div>
              <div class="col-lg-6 col-sm-6 col-6 header-top-right">
              <div class="header-social">
                <a href="https://www.facebook.com/Bintour-2309663692425766/"><i class="fab fa-facebook"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
             
              </div>
              </div>

            </div>                  
          </div>
        </div>
        <div class="container main-menu">
          <div class="row align-items-center justify-content-between d-flex">
              <div id="logo">
                <a href="index.php"><img src="../img/logo.png" alt="" title="" /></a>
              </div>
              <nav id="nav-menu-container">

                <ul class="nav-menu">


                  <li><a href="index.php">Inicio <i class="fas fa-home"></i></a></li>
                  <li><a href="noti.php">Noticias</a><i class="fas fa-newspaper-o"></i></li>
                  <li><a href="acercade.php">Acerca de <i class="fas fa-question-circle">  </i></a></li>
                  <li><a href="misviajes.php">Mis Viajes<i class="fas fa-plane"></i></a></li>
                  <?php if (isset($_SESSION["id"])): ?>
                    <?php    $iduser= $_SESSION["id"];
                     if ($iduser==2): ?>
                      
                  <li><a href="nuevopaquete.php">Paquetes <i class="fas fa-archive"> </i></a></li>
                    <?php endif ?>
      
                    
                  <?php endif ?>
                  <li><a href=""></a></li>
                  <li><a href="contacto.php">Contacto</a></li>
                  <li><a href="propuesta.php">Proponer Viaje</a></li>
    
                    <?php   
       
                      if (isset($_SESSION['usuario'])) {
                      
                 ?> 
                  <li>  <a href=" "><?php echo $_SESSION['usuario']; ?></a></li>
                  <li>  <a href="controladores/cerrar.php">Cerrar<i class=" fas fa-power-off">  </i></a> </li>
                 <?php  

                    } else{?>
                   <li><a href="login.php">Iniciar sesión</a></li>
                   <li><a href="registro.php">Registrar</a></li>
                <?php   } ?>

                </ul>

              </nav><!-- #nav-menu-container -->                      
          </div>
        </div>
      </header><!-- #header -->

       

<script>
$(document).ready(function(){
 
 function load_unseen_notification(view = '')
 {
  var paque=1;
  var user=<?php  if (isset($_SESSION['id'])) {
    echo $_SESSION['id'];
  }else{
  echo "1"; }?>;
  $.ajax({
   url:"fetch.php",
   method:"POST",
   data:{view:view,paque:paque,user:user},
   dataType:"json",
   success:function(data)
   {
    $('.dropdown-menu').html(data.notification);
    if(data.unseen_notification > 0)
    {
     $('.count').html(data.unseen_notification);
    }
   }
  });
 }
 
 load_unseen_notification();
 
 $('#comment_form').on('submit', function(event){
  event.preventDefault();
  if($('#subject').val() != '' && $('#comment').val() != '')
  {
   var form_data = $(this).serialize();
   $.ajax({
    url:"insert.php",
    method:"POST",
    data:form_data,
    success:function(data)
    {
     $('#comment_form')[0].reset();
     load_unseen_notification();
    }
   });
  }
  else
  {
    alert('Campos Obligatorios');
  }
 });
 
 $(document).on('click', '.dropdown-toggle', function(){
  $('.count').html('');
  load_unseen_notification('yes');
 });
 
 setInterval(function(){ 
  load_unseen_notification();; 
 }, 5000);
 
});
</script><?php 

if (isset($_SESSION["id"])){
        $iduser= $_SESSION["id"];
        if ($iduser==2) {
        	# code...
        }else{
       header('Location: index.php');
        }
    }else{
 header('Location: login.php');//Aqui lo redireccionas al lugar que quieras.
     die() ;

    }
 ?>
<header class="bg-dark text-center py-5 mb-4" style="height: 200px">
  <div class="container ">
  	<br>	<br>	<br>	
    <h1 class="font-weight-light text-white">Paquetes</h1>
  </div>
</header>

<div class="container">
<div class="row">

<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus-square"></i>
 Agregar Paquete
</button>
<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal2"><i class="fas fa-plus-square"></i>
Propuesta de paquetes
</button>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="./controladores/agregarpropuesta.php">  
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva Propuesta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    <label> Que quieres preguntar?</label>
        <input type="text" name="propuesta" class="form-control" placeholder="¿Deseas Viajar a algun lugar?" required/>
        <label> Opcion 1</label>
        <input type="text" name="opcion1" class="form-control" placeholder="opcion1: Mexico" required/>
        <label> Opcion 2</label>
        <input type="text" name="opcion2" class="form-control" placeholder="opcion2: Teotihucan" required/>
        <label> Opcion 3</label>
        <input type="text" name="opcion3" class="form-control" placeholder="opcion3: Guadalajara" required/>
        <label> Fecha limite para mostrar la propuesta</label>
        <input type="date" name="fecha_limite" class="form-control" required/>
  <hr>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
         <button type="submit" class="btn btn-success" >Agregar</button>
     </div>
      </form>
      </div>
    
    </div>
  </div>
<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModa"><i class="fas fa-plus-square"></i>
Nueva Noticia
</button>


</div> 
<hr>  





<div class="">
	<table class="table hover " >
		<thead align="center">
			<tr>
				<th>No.</th>
				<th>Destino</th>
				
				<th>Pasajeros</th>
				<th>Acción</th>
			</tr>
		</thead>
		<tbody align="center">
			<?php 

$server=new MySQL();
$datos=$server->EjecutarSQL("SELECT * FROM paquetes ");
$i=1;
 while($rf = mysqli_fetch_assoc($datos)){
	$id_paquetes=$rf['id_paquete'];
	$destino=$rf['destino'];
	$info=$rf['informacion'];
	$dispo=$rf['disponibles'];
	$boletos=$rf['boletos'];
		                                               
	 ?>
			
			<tr>
				<td><?php 	echo $i; ?>- <?php echo $id_paquetes; ?></td>
				<td><?php 	echo $rf['destino']; ?></td>
				
				<td><?php echo $boletos-$dispo;  ?>/<?php echo $boletos;	 ?></td>
				<td><!--<a href="" class="btn btn-info btn-xs btn-circle"><i class="fas fa-pen btn-circle"></i></a> --> <a href="./controladores/eliminarpaquete.php?eli=<?php echo $rf['id_paquete']; ?>" class="btn btn-danger btn-xs btn-circle"><i class="fas fa-trash">	</i></a>
          <a  class="btn btn-warning" data-toggle="modal" data-target="#exampleModal<?php echo $id_paquetes;   ?>"><i class="fas fa-plus"></i>
</a>
<a href="listaviajeros.php?paqu=<?php echo $id_paquetes; ?>" class="btn-success btn"><i class="fas fa-file-download"></i></a>
 <div class="modal fade" id="exampleModal<?php echo $id_paquetes;    ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="post" id="comment_form"> 
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo aviso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    <label> Aviso</label>
         <input type="text"  name="id_paquete" value="<?php echo $id_paquetes;?>" >
    <div class="form-group">
     <label>Asunto</label>
     <input type="text" name="subject" id="subject" class="form-control">
    </div>
    <div class="form-group">
     <label>Detalle</label>
     <textarea name="comment" id="comment" class="form-control" rows="5"></textarea>
    </div>
   

   
  <hr>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
         <button type="submit" class="btn btn-success" >Agregar</button>
     </div>
    </form>
      </div>
    
    </div>
  </div>
				
		

			
				</td>
			</tr>
			<?php $i++;	} ?>
		</tbody>
	</table>
</div>




  <!-- Button trigger modal -->

				    
				      		<div class="alert alert-info" role="alert">Usuarios Interesados en viajar</div>
				      		
				      			<table class="table table-striped">
				      				<thead>
				      					<tr>
				      					<th>Nombre</th>
				      					<th>Solicitud</th>
				      					<th>Destino</th>
				      					<th>Estado</th>
                        <th>Cantidad</th>
				      					<th>Accion</th>
				      				</tr>
				      				</thead>
				      			<?php $solsql="SELECT DISTINCT solicitud.* , usuarios.*, paquetes.* from paquetes, usuarios,solicitud WHERE solicitud.id_usuarios=usuarios.id and solicitud.id_paquetes=paquetes.id_paquete ORDER BY id_solicitud DESC";
								$so=$server->EjecutarSQL($solsql);						
				      			 while($rsoli = mysqli_fetch_assoc($so)){
				      			 $id_usuarios=$rsoli['id'];
				      			 $id_solicitud=$rsoli['id_solicitud'];
								 $usuario=$rsoli['usuario'];
								 $estado=$rsoli['status'];
								 $destino=$rsoli['destino'];
								 $referencia=$rsoli['referencia'];
								 $soli=$rsoli['id_solicitud'];
                 $cantidad=$rsoli['cantidad_boletos'];
                 $disponibles=$rsoli['disponibles'];

				      			 ?>
				      			 <tr>
				      			 	<td><?php echo $usuario; ?> </td>
				      			 	<td><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal<?php echo $id_solicitud?>"><i class="fas fa-file-download"></i>
									ver archivo
									</button>
							
										<div class="modal fade" id="modal<?php echo $id_solicitud;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">
									    <div class="modal-content">
									    	
									      <div class="modal-header">
									        <h5 class="modal-title" id="exampleModalLabel">Referencia</h5>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">&times;</span>
									        </button>
									      </div>
									      <div class="modal-body">
									 
									<object type="application/pdf" data="controladores/archivos/<?php echo $referencia; ?>" width="100%" height="500" style="height: 85vh;">No Support</object>
										<hr>	
									      </div>
									      <div class="modal-footer">
									        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
									      
									      </div>
									    
									    </div>
									  </div>
									</div>  
								 
									</td>
				      			 	<td>	<?php 	echo $destino; ?></td>
				      			 	<td class="<?php 	if($estado=='espera'){ echo 'btn-warning';}else if($estado=='aceptado'){echo 'btn-success';}else if($estado=='rechazado'){ echo 'btn-danger';} ?>"><?php echo $estado; ?></td>
                      <td>  <?php   echo $cantidad; ?></td>
				      			 	<td><a href="./controladores/status.php?var=accept&&id=<?php echo $soli ?>&&cant=<?php   echo $disponibles-$cantidad; ?>" class="btn-success btn"><i class="fas fa-check"></i></a><a href="./controladores/status.php?var=delete&&id=<?php echo $soli; ?>" class="btn btn-danger"><i class="fas fa-times"></i></a>

                      </td>
				      			 	
				      			 </tr>
				      			<?php } ?>
				      			</table>
				      	


				      		</div>


				  
				<!--MODAL PARA AGREGAR PROPUESTA-->


 

  <div class="modal fade" id="exampleModa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form method="POST" action="./controladores/agregarnoticia.php">	
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva Noticia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 		<label>Titulo de la Noticia</label>
      	<input type="text" name="titulo" class="form-control" placeholder="Ejm: Sabias que?" required/>
      	<label>	Contenido de la noticia</label>
      	<textarea type="text" name="contenido" class="form-control"  maxlength="2000" required>
      	</textarea>
      	<label>	Selecciona un Paquete</label>
      	<select  name="paquete" class="form-control">
      		<option disabled hidden >Selecciona</option>
      		   <?php 
  

$datos=$server->EjecutarSQL("SELECT * FROM  paquetes WHERE  fecha_regreso>=now()");

 while($rf = mysqli_fetch_assoc($datos)){
  $id_paquete=$rf['id_paquete'];
  $destino=$rf['destino'];


       ?> 
       <option hidden selected disabled>Selecciona</option> 
       <option value="<?php echo $id_paquete	 ?>">	<?php echo $destino; ?></option>
   <?php 	} ?>



      	</select>
    
      	<label>	Fecha limite para mostrar la noticia</label>
      	<input type="date" name="fecha_limite" class="form-control" required/>
	<hr>	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
         <button type="submit" class="btn btn-success" >Agregar</button>
     </div>
      </form>
      </div>
    
    </div>
  </div>

<!-- Modal PARA AGREGAR PAQUERE-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="./controladores/agregarpaquete.php" method="POST" enctype="multipart/form-data">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Paquete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	<div>
		
		<label>Destino: </label>
		<input type="text" name="destino" class="form-control" placeholder="Ejemplo: Grutas Tolantongo" required/>
	</div>
	<div>
		<label>Boletos: </label>
		<input type="number" min="1" name="boletos" class="form-control" placeholder="Numero de asientos disponibles" required />
	</div>	
	<div>
		<label>Costo: </label>
		<input type="number" min="1" name="costo" class="form-control" placeholder="Costo" required />
	</div>
	<div>
		<label>Tipo: </label>
		<select  name="tipo_paquete" class="form-control" required >
			<option selected disabled hidden>Selecciona</option>
			<option value="Playa">Playa</option>
			<option value="pueblo_magico">Pueblo Magico</option>
			<option value="educativo">Educativo</option>

		</select>
	</div>
	<div>
		
		<label>Estado: </label>
		<input type="text" name="estado" class="form-control" placeholder="Ejemplo: Hidalgo" required/>
	</div>
	<div>
		<label>Fecha de Salida: </label>
		<input type="date" name="fecha_salida" class="form-control" required />
	</div>
	<div>
		<label>Fecha de regreso: </label>
		<input type="date" name="fecha_regreso" class="form-control" required />
	</div>
	<div>
		<label>Informacion General: </label>
		<textarea type="text" name="info" class="form-control" placeholder="Recorrido, que incluye el viaje, etc." required></textarea>
	</div>
	<div>
		<label>Imagen</label>
		<input type="file" name="imagen" class="form-control " id="file-upload" accept="image/*" required/>
		<div id="file-preview-zone" align="center" style="min-width: 100%" >
        </div>

	</div>
	<hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
      </div>
      </form>
    </div>
  </div>
</div>

  





<script type="text/javascript">
function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
 
            reader.onload = function (e) {
                var filePreview = document.createElement('img');
                filePreview.id = 'file-preview';
                //e.target.result contents the base64 data from the image uploaded
                filePreview.src = e.target.result;
                console.log(e.target.result);
 
                var previewZone = document.getElementById('file-preview-zone');
                previewZone.appendChild(filePreview);
            }
 
            reader.readAsDataURL(input.files[0]);
        }
    }
 
    var fileUpload = document.getElementById('file-upload');
    fileUpload.onchange = function (e) {
        readFile(e.srcElement);
    }
   
</script>




<?php   $grafi=$server->EjecutarSQL("SELECT propuestas.*, userpropuesta.otro, usuarios.usuario FROM `propuestas`,`userpropuesta`,`usuarios` WHERE usuarios.id=userpropuesta.id_usuario and userpropuesta.otro!=''");
$i=0;
 while($gr = mysqli_fetch_assoc($grafi)){
  $id_propuesta=$gr['id_propuesta'];
  $propuesta_destino=$gr['propuesta_destino'];
  $opcion1=$gr['opcion1'];
  $opcion2=$gr['opcion2'];
  $opcion3=$gr['opcion3'];
  $otr=$gr['otro'];
   $usu=$gr['usuario'];
  
      $uno="SELECT  resultado from userpropuesta WHERE id_propuesta=$id_propuesta and resultado=1";
      $reuno=$server->EjecutarSQL($uno);
      $resuluno=mysqli_num_rows($reuno);
      $dos="SELECT  resultado from userpropuesta WHERE id_propuesta=$id_propuesta and resultado=2";
      $redos=$server->EjecutarSQL($dos);
      $resuldos=mysqli_num_rows($redos);
      $tres="SELECT  resultado from userpropuesta WHERE id_propuesta=$id_propuesta and resultado=3";
      $retres=$server->EjecutarSQL($tres);
      $resultres=mysqli_num_rows($retres);
      $reotro="SELECT  resultado from userpropuesta WHERE id_propuesta=$id_propuesta and resultado=4";
      $ress=$server->EjecutarSQL($reotro);
      $ra=mysqli_num_rows($ress);

   ?>

    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Destino', 'Interesados'],
          ['<?php   echo $opcion1; ?>', <?php echo $resuluno;?>],
          ['<?php   echo $opcion2; ?>',  <?php echo $resuldos;?>],
          ['<?php   echo $opcion3 ?>',  <?php  echo $resultres;?>],
          ['Otro',  <?php  echo $ra;?>]

         
        ]);

        var options = {
          title: '<?php echo $propuesta_destino; ?>'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart<?php echo $id_propuesta;?>'));

        chart.draw(data, options);
      }
    </script>
   <div class="container">
      <div class="alert alert-success" role="alert">Resultados de Viajes Propuestos</div>
      <div id="piechart<?php echo $id_propuesta;  ?>" style="width: 900px; height: 500px;"></div>
   </div>

                <?php  } 
               

                ?>

                <?php  ?>
    <div class="container">
      <div class="alert alert-info" role="alert">Resultados de Propuestos Otros</div>
       <table class="table">
      <th>NO.</th>
      <th>Destino Propuesto</th>
       <th>Usuario</th>
    <tbody>
<?php $i=1;
  $grafi=$server->EjecutarSQL("SELECT propuestas.*, userpropuesta.otro, usuarios.usuario FROM `propuestas`,`userpropuesta`,`usuarios` WHERE usuarios.id=userpropuesta.id_usuario and userpropuesta.otro!=''");
$i=1;
 while($gr = mysqli_fetch_assoc($grafi)){
  $id_propuesta=$gr['id_propuesta'];
  $propuesta_destino=$gr['propuesta_destino'];
  $opcion1=$gr['opcion1'];
  $opcion2=$gr['opcion2'];
  $opcion3=$gr['opcion3'];
  $otr=$gr['otro'];
   $usu=$gr['usuario']; ?>
      <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $otr; ?> </td>
        <td><?php echo $usu; ?> </td>
      </tr>
    <?php $i++; } ?>
    </tbody>
    </table>

    </div>
   <?php ob_end_flush();  ?>