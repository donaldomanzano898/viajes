<?php include 'head.php'; ?>

<header class="bg-dark text-center py-5 mb-4" style="height: 200px">
  <div class="container ">
    <br>  <br>  <br>  
    <h1 class="font-weight-light text-white">Conocenos</h1>
  </div>
</header>

<div class="container">
  <div class="row">
    <!-- Team Member 1 -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
        <img src="../img/c2.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Barcena Rojas Leonel</h5>
          <div class="card-text text-black-50">CEO</div>
        </div>
      </div>
    </div>
    <!-- Team Member 2 -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
        <img src="../img/c1.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">González Rodarte Oscar Omar</h5>
          <div class="card-text text-black-50">oscaromarrodarte@gmail.com</div>
           <div class="card-text text-black-50">CIO</div> 
        </div>
      </div>
    </div>
    <!-- Team Member 3 -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
        <img src="../img/c3.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Simón Martin Blanca Estela</h5>
          <div class="card-text text-black-50">blancasimon.m20@gmail.com</div>
          <div class="card-text text-black-50">fd</div>
        </div>
      </div>
    </div>
    <!-- Team Member 4 -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
        <img src="../img/c5.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Tsdfsjkbsdfbc</h5>
          <div class="card-text text-black-50">sdf</div>
          <div class="card-text text-black-50">bl</div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->

</div>
<?php include 'footer.php'; ?>