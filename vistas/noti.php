<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>	Noticias</title>
</head>
<body>
	 <link rel="stylesheet" href="./OPENCODE_files/main.css">
    
    <link rel="shortcut icon" href="./OPENCODE_files/o.png">
		<!-- Header -->
		<link rel="stylesheet" href="../OPENCODE_files/main.css">
    
    <link rel="shortcut icon" href="../OPENCODE_files/o.png">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">	
<link rel="stylesheet" type="text/css" href="../css/fot.css">	
<link rel="stylesheet" type="text/css" href="../css/misestilos.css">  	
<link rel="stylesheet" href="../font/css/all.css"/>
<link rel="shortcut icon"  href="../img/logo1.png">


  <link rel="stylesheet" href="../css/linearicons.css">
      <link rel="stylesheet" href="../css/font-awesome.min.css">
      <link rel="stylesheet" href="../css/magnific-popup.css">
      <link rel="stylesheet" href="../css/jquery-ui.css">        
      <link rel="stylesheet" href="../css/nice-select.css">              
      <link rel="stylesheet" href="../css/animate.min.css">
      <link rel="stylesheet" href="../css/owl.carousel.css">       
      <link rel="stylesheet" href="../css/main.css">
      <link rel="stylesheet" type="text/css" media="screen" href="../css/estilo.css"/>




<script src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/loader.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/popper.min.js"></script>  
<script src="../js/svgembedder.min.js"></script>     
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>   
<script src="../js/jquery-ui.js"></script>         
<script src="../js/easing.min.js"></script>      
<script src="../js/hoverIntent.js"></script>
<script src="../js/superfish.min.js"></script> 
<script src="../js/jquery.ajaxchimp.min.js"></script>
<script src="../js/jquery.magnific-popup.min.js"></script>           
<script src="../js/jquery.nice-select.min.js"></script>          
<script src="../js/owl.carousel.min.js"></script>              
<script src="../js/mail-script.js"></script> 
<script src="../js/main.js"></script>


		 <header id="header">
        <div class="header-top">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-6 col-sm-6 col-6 header-top-left">
                <ul>
                  <li><a href="#">Pueblos Magicos</a></li>
                  <li><a href="#">Viajes Redondos</a></li>
                </ul>     
              </div>
              <div class="col-lg-6 col-sm-6 col-6 header-top-right">
              <div class="header-social">
                <a href="https://www.facebook.com/Bintour-2309663692425766/"><i class="fab fa-facebook"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
             
              </div>
              </div>
              <div class="container main-menu">
          <div class="row align-items-center justify-content-between d-flex">
              <div id="logo">
                <a href="index.php"><img src="../img/logo.png" alt="" title="" /></a>
              </div>
              <nav id="nav-menu-container">
                <ul class="">
                  <li><a href="index.php">Inicio<i class="fas fa-home"></i></a></li>
                  <li><a href="noti.php">Noticias</a><i class="fas fa-newspaper-o"></i></li>
                  <li><a href="acercade.php">Acerca de<i class="fas fa-question-circle">  </i></a></li>
                  <li><a href="misviajes.php">Mis Viajes<i class="fas fa-plane"></i></a></li>
                  <?php if (isset($_SESSION["id"])): ?>
                    <?php    $iduser= $_SESSION["id"];
                     if ($iduser==2): ?>
                      
                  <li><a href="nuevopaquete.php">Paquetes <i class="fas fa-archive"> </i></a></li>
                    <?php endif ?>
      
                    
                  <?php endif ?>
                  <li><a href=""></a></li>
                  <li><a href="contacto.php">Contacto</a></li>
                  <li><a href="propuesta.php">Proponer Viaje</a></li>
    
                    <?php   
       
                      if (isset($_SESSION['usuario'])) {
                      
                 ?> 
                  <li>  <a href=" "><?php echo $_SESSION['usuario']; ?></a></li>
                  <li>  <a href="controladores/cerrar.php">Cerrar sesión<i class=" fas fa-power-off">  </i></a> </li>
                 <?php  

                    } else{?>
                   <li><a href="login.php">Iniciar sesión</a></li>
                   <li><a href="registro.php">Registrar</a></li>
                <?php   } ?>

                </ul>
              </nav><!-- #nav-menu-container -->                      
          </div>
        </div>
            </div>                  
          </div>
        </div>
        
      </header><!-- #header -->


		<!-- Intro -->
			<section id="intro" class="main style1 dark fullscreen" style="height:100%">
				<div class="content container 55%">
					<header>
						<h2>Bienvenidos.</h2>
					</header>
				<h3>		<p>Bienvenidos a la era donde la <strong>Tecnología</strong>,  nos permite <strong>Viajar</strong> a diferentes sitios <strong>Turisticos</strong> de todo México</p>
					<footer></h3>
						<a href="#one" class="button style2 down">More</a>
					</footer>
				</div>
			</section>
             
		<!-- Nosotros -->
    <?php 
  require_once("controladores/conexion/MySQL.php");
$server=new MySQL();
$datos=$server->EjecutarSQL("SELECT paquetes.imagen, noticias.* from noticias INNER JOIN paquetes WHERE paquetes.fecha_regreso>=now()");

 while($rf = mysqli_fetch_assoc($datos)){
  $id_noticia=$rf['id_noticia'];
  $titulo_noticia=$rf['titulo_noticia'];
  $contenido_noticia=$rf['contenido_noticia'];
  $idpa=$rf['id_paquete'];
   $imagen=$rf['imagen'];

       ?>   
       <?php  if ($id_noticia%2==0){
    ?>
<section id="one" class="main style2 right dark fullscreen inactive" style="height:100%">
        <div class="content box style2">
        
          <header>
            <h1><strong><?php   echo $titulo_noticia;?></strong></h1>
          </header>
      <h4>      <p align="justify"><?php  echo $contenido_noticia; ?></p></h4>
      <div> 
 <img class="img-fluid" src="controladores/archivos/<?php echo $imagen;  ?>" alt="">
      </div>
      <div class="desc"> <a href="detallepaquete.php?paquete=<?php echo $idpa ?>" class="price-btn">Ver mas...</a>  </div>
        </div>
        
        <a href="#two" class="button style2 down anchored">Next</a>

        </section>

    <?php
}else{
    ?>

    <!-- Tecnología -->
      <section id="two" class="main style2 left dark fullscreen inactive">
        <div class="content box style2">
          <header>
            <h2><strong>  <?php   echo $titulo_noticia; ?></strong></h2>
          </header>
          <p align="justify"><?php  echo $contenido_noticia; ?> </p>
                   <div> 
 <img class="img-fluid" src="controladores/archivos/<?php echo $imagen;  ?>" alt="">
      </div>
      <div class="desc"> <a href="detallepaquete.php?paquete=<?php echo $idpa ?>" class="price-btn">Ver mas...</a>  </div>
        </div>
        
      </section>

    <?php
} 
}?>
			
          
			

		
        
            
     
            
	
        
            
      <script src="./OPENCODE_files/jquery.min.js.descarga"></script>
      <script src="./OPENCODE_files/jquery.poptrox.min.js.descarga"></script>
      <script src="./OPENCODE_files/jquery.scrolly.min.js.descarga"></script>
      <script src="./OPENCODE_files/jquery.scrollex.min.js.descarga"></script>
      <script src="./OPENCODE_files/skel.min.js.descarga"></script>
      <script src="./OPENCODE_files/util.js.descarga"></script>
      <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
      <script src="./OPENCODE_files/main.js.descarga"></script>
            
		

		<!-- Scripts -->
	

	
<div class="poptrox-overlay" style="position: fixed; left: 0px; top: 0px; z-index: 10001; width: 100%; height: 100%; text-align: center; cursor: pointer; display: none;"><div style="display:inline-block;height:100%;vertical-align:middle;"></div><div style="position:absolute;left:0;top:0;width:100%;height:100%;background:#1f2328;opacity:0.65;filter:alpha(opacity=65);"></div><div class="poptrox-popup" style="display: none; vertical-align: middle; position: relative; z-index: 1; cursor: auto; min-width: 200px; min-height: 100px;"><div class="loader" style="display: none;"></div><div class="pic" style="display: none;"></div><div class="caption" style="display: none;"></div><span class="closer" style="cursor: pointer; display: none;">×</span><div class="nav-previous" style="display: none;"></div><div class="nav-next" style="display: none;"></div></div></div>

</body>

            <div class="row">
            
                <!-- Grid column -->
                <div class="col-md-12">
                    
                    <!--Footer-->
                    <footer class="page-footer negro center-on-small-only pt-0">

                        <!--Footer Links-->
                        <div class="container">

                            <!--Grid row-->
                            <div class="row pt-5 mb-3 text-center d-flex justify-content-center">

                                <!--Grid column-->
                                <div class="col-md-2 mb-3">
                                    <h6 class="title font-bold"><a href="#!">Acerca de...</a></h6>
                                </div>
                                <!--Grid column-->

                                <!--Grid column-->
                                <div class="col-md-2 mb-3">
                                    <h6 class="title font-bold"><a href="#!">Paquetes</a></h6>
                                </div>
                                <!--Grid column-->

                                <!--Grid column-->
                                <div class="col-md-2 mb-3">
                                    <h6 class="title font-bold"><a href="#!">Valoración</a></h6>
                                </div>
                                <!--Grid column-->

                                <!--Grid column-->
                                <div class="col-md-2 mb-3">
                                    <h6 class="title font-bold"><a href="#!">Ayuda</a></h6>
                                </div>
                                <!--Grid column-->

                                <!--Grid column-->
                                <div class="col-md-2 mb-3">
                                    <h6 class="title font-bold"><a href="contacto.php">Contacto</a></h6>
                                </div>
                                <!--Grid column-->

                            </div>
                            <!--Grid row-->

                            <hr class="rgba-white-light" style="margin: 0 15%;">

                            <!--Grid row-->
                            <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

                                <!--Grid column-->
                                <div class="col-md-8 col-12 mt-5">
                                    <p style="line-height: 1.7rem">Brindar un servicio de calidad y confiabilidad, por medio de los paquetes de viaje que el cliente elije, para superar sus expectativas a través de viajes únicos ofreciendo una buena atención por parte nuestra organización, con precios accesibles.   </p>
                                    

                                </div>
                                <!--Grid column-->

                            </div>
                            <!--Grid row-->

                            <hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">

                            <!--Grid row-->
                            <div class="row pb-3">

                                <!--Grid column-->
                                <div class="col-md-12">

                                    <div class="footer-socials mb-5 flex-center">
                                        <!--Facebook-->
                                        <a href="https://www.facebook.com/Bintour-2309663692425766/" class="icons-sm fb-ic"><i class="fab fa-facebook fa-lg white-text mr-md-4"> </i></a>
                        
                                        <!--Instagram-->
                                        <a class="icons-sm ins-ic"><i class="fab fa-instagram fa-lg white-text mx-md-4"> </i></a>
                                    
                                    </div>
                                </div>
                                <!--Grid column-->
                            </div>
                            <!--Grid row-->

                        </div>
                        <!--/Footer Links-->

                        <!--Copyright-->
                        <div class="footer-copyright">
                            <div class="container-fluid">
                                <?php   echo date("Y"); ?>Copyright: <a href="#">BinTour.com</a>
                            </div>
                        </div>
                        <!--/Copyright-->

                    </footer>
                    <!--/Footer-->
                                    
                </div>
                <!-- Grid column -->
            
            </div>
</html>